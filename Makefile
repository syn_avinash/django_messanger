docker_build:
	docker-compose up -d db
	docker-compose up -d nginx_server

docker_restart:
	docker-compose stop
	docker-compose up -d

docker_stop:
	docker-compose stop

test:
	python manage.py test

run_server:
	python manage.py runserver 0:8000

make_migration:
	python manage.py makemigrations

show_migrations:
	python manage.py showmigrations

celery:
	celery -A django_messanger worker -l debug -Q local

shell:
	python manage.py shell


gunicorn:
	conf/gunicorn_dev.sh

asgi:
	daphne django_messanger.asgi:application --bind 0.0.0.0 --port 9000 --verbosity 1 --access-log logs/asgi_log.log
