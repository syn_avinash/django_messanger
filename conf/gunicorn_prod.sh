#!/bin/bash
NAME="DJANGO_MESSANGER"
DJANGODIR=/code
SOCKFILE=/code/run/gunicorn.sock
NUM_WORKERS=5
TIMEOUT=300
DJANGO_WSGI_MODULE=django_messanger.wsgi
echo "Starting $NAME as `whoami`"
cd $DJANGODIR
export DJANGO_SETTINGS_MODULE=django_messanger.setting_files.production
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
echo $PWD
#exec python manage.py makemigrations
#exec python manage.py migrate
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --bind=0.0.0.0:6500\
  --log-level=debug \
  --log-file=logs/gunicorn.log