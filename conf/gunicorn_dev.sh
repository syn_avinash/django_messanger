#!/bin/bash
NAME="DJANGO_MESSANGER"                                  # Name of the application
DJANGODIR=~/code/django_messanger            # Django project directory
SOCKFILE=~/code/django_messanger/run/gunicorn.sock  # we will communicte using this unix socket
NUM_WORKERS=5                                     # how many worker processes should Gunicorn spawn
TIMEOUT=300
#DJANGO_SETTINGS_MODULE=django_messanger.settings.local             # which settings file should Django use
DJANGO_WSGI_MODULE=django_messanger.wsgi                     # WSGI module name
DJANGO_ASGI_MODULE=django_messanger.asgi                     # ASGI module name
echo "Starting $NAME as `whoami`"
# Activate the virtual environment
cd $DJANGODIR
source ~/p3env/bin/activate #project virtualenv
export DJANGO_SETTINGS_MODULE=django_messanger.setting_files.development
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR
#echo $PWD
#echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

#start cmd commnad as:
#gunicorn django_messanger.wsgi:application --name CPD --workers 3 --bind=0.0.0.0:8010  --log-level=debug --log-file=gunicorn.log

# Start your Django Guicorn

# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --bind=127.0.0.1:6500\
  --log-level=debug \
  --log-file=logs/gunicorn.log
#echo "before"
#exec daphne ${DJANGO_ASGI_MODULE}:application --bind 127.0.0.1 --port 9000 --verbosity 1 --log-file logs/daphne.log
#echo "after"