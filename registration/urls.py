from django.conf.urls import url
from registration import views
from registration.views import *

urlpatterns = [
    url(r'^$', views.user_login, name='home'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^home/$', views.home, name='home'),
    url(r'^logout/$', views.usr_logout, name='logout'),
    url(r'^custom/notif/$', views.notif, name='notif'),
    url(r'^custom/mark_read/(?P<slug>\d+)/$', views.mark_read, name='mark_read'),
    url(r'^friends/$', views.contacts, name='mark_read'),
    url(r'^isonline/$', views.isonline),
]
