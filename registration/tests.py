from django.test import TestCase
from registration.models import UserProfile
from django.contrib.auth.models import User
from django.test import Client


class CreateUser(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        User.objects.create(username="avinash", first_name="Avinash", last_name="Thorat")
        User.objects.create(username="dinesh", first_name="Dinesh", last_name="Gawali")

    def test_user(self):
        usr1 = User.objects.get(username="avinash")
        self.assertTrue(isinstance(usr1, User))
        self.assertEqual(usr1.get_username(), usr1.username)

    def test_user_profile(self):
        usr1 = User.objects.get(username="avinash")
        usr2 = UserProfile.objects.get(user=usr1)
        self.assertEqual(usr1, usr2.user)

    def test_login(self):
        res = [200, 302]
        res_login = self.client.post('/', {'uname': 'dinesh', 'psw': 'password'})
        self.assertIn(res_login.status_code, res)

    def test_home(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/home/')
        self.assertEqual(response.status_code, 200)
