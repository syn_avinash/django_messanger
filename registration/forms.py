from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'type': 'text', 'class': 'form-control',
                                                                            'name': 'username', 'id': 'username',
                                                                            'placeholder': 'Enter your Username'}))

    first_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'type': 'text', 'class': 'form-control',
                                                                              'name': 'first_name', 'id': 'first_name',
                                                                              'placeholder': 'First Name'}))

    last_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'type': 'text', 'class': 'form-control',
                                                                             'name': 'last_name', 'id': 'last_name',
                                                                             'placeholder': 'Last Name'}))

    email = forms.EmailField(max_length=254, widget=forms.TextInput(attrs={'type': 'text', 'class': 'form-control',
                                                                           'name': 'email', 'id': 'email',
                                                                           'placeholder': 'Enter your Email'}))

    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'type': 'password', 'class': 'form-control',
                                                                  'name': 'password', 'id': 'password',
                                                                  'placeholder': 'Enter your Password'}))

    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'type': 'password', 'class': 'form-control',
                                                                  'name': 'confirm', 'id': 'confirm',
                                                                  'placeholder': 'Confirm your Password'}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',)
