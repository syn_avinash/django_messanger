from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class TrackDateModel(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserProfile(TrackDateModel):
    user = models.OneToOneField(User)
    fullname = models.CharField(max_length=200)
    date_of_birth = models.DateField(null=True)

    def __str__(self):
        return self.user


class PendingFriendship(TrackDateModel):
    from_user = models.ForeignKey(User, related_name="request_from")
    to_user = models.ForeignKey(User, related_name="request_to")
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.from_user


class Friendship(TrackDateModel):
    friendship_id = models.AutoField(primary_key=True, unique=True)
    current_user = models.ForeignKey(User, related_name="current_user")
    friend_user = models.ForeignKey(User, related_name="friend_user")
    following = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.friendship_id


class LoggedInUser(TrackDateModel):
    user = models.OneToOneField(User, related_name='logged_in_user')
    def __unicode__(self):
        return self.user

def create_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


post_save.connect(create_profile, sender=User, dispatch_uid="user_profile")
