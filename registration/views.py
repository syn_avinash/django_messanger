from registration.models import Friendship
from django.db.models import Q
from django.contrib.auth import login, authenticate, logout
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_protect
from chatapp.models import ChattingData
from registration.forms import SignUpForm
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.test import Client
from notifications.utils import id2slug, slug2id
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404
from notifications.models import Notification
from django.contrib.auth.models import User


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            # __________Future_Scope________
            # username = form.cleaned_data.get('username')
            # raw_password = form.cleaned_data.get('password1')
            # user = authenticate(username=username, password=raw_password)
            # login(request, user)
            return redirect("/")
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})


@login_required(login_url="/")
def home(request):
    alluser = Friendship.objects.filter(current_user=request.user, is_active=True)
    distinct_chat_ids = ChattingData.objects.filter(Q(text_from=request.user) |
                                                    Q(text_to=request.user)).values_list("chat_id").order_by(
        "-create_date")

    f_set = set()
    chat_records = list()
    for distinct_chat_id in distinct_chat_ids:
        if distinct_chat_id not in f_set:
            chat_data = ChattingData.objects.filter(chat_id=distinct_chat_id[0]).last()
            if request.user.id == chat_data.text_to_id:
                chat_data.online = 'true' if hasattr(chat_data.text_from, 'logged_in_user') else 'false'
            else:
                chat_data.online = 'true' if hasattr(chat_data.text_to, 'logged_in_user') else 'false'
            f_set.add(distinct_chat_id)
            chat_records.append(chat_data)
        else:
            pass
    for usr in alluser:
        usr.online = 'true' if hasattr(usr.friend_user, 'logged_in_user') else 'false'
    # alluser = User.objects.filter(is_active=True, is_superuser=False).exclude(id=request.user.id)

    notifications_data = request.user.notifications.unread()
    notif_count = len(notifications_data)
    res_list = list()
    for notif_data in notifications_data:
        res_dict = model_to_dict(notif_data)
        usr = User.objects.get(id=notif_data.actor_object_id)
        res_dict["first_name"] = usr.first_name
        res_dict["last_name"] = usr.last_name
        res_dict["slug"] = id2slug(notif_data.id)
        res_list.append(res_dict)
    return render(request, 'registration/home.html', {"reg_users": alluser, "data": [], "chat_records": chat_records,
                                                      "notifications": res_list, "notif_count": notif_count})


@csrf_protect
def user_login(request):
    if request.user.is_authenticated():
        return redirect('/home')
    elif request.method == "POST":
        username = request.POST['uname']
        raw_password = request.POST['psw']
        user = authenticate(username=username, password=raw_password)
        if user:
            login(request, user)
            return redirect('/home')
            # return redirect(request, 'registration/home.html',)
        else:
            return render(request, 'registration/login.html', {"error": "invalid username or password"})
    return render(request, 'registration/login.html')


@login_required(login_url="/")
def usr_logout(request):
    logout(request)
    request.session.flush()
    return redirect("/")


@login_required(login_url="/")
def notif(request):
    # client=Client()
    # client.force_login(request.user)
    # res1 = client.get('/inbox/notifications/unread/')
    dd = request.user.notifications.unread()
    res_list = list()
    for i in dd:
        res_dict = model_to_dict(i)
        usr = User.objects.get(id=i.actor_object_id)
        res_dict["first_name"] = usr.first_name
        res_dict["last_name"] = usr.last_name
        res_dict["slug"] = id2slug(i.id)
        res_list.append(res_dict)
    return JsonResponse(res_list, safe=False)


@login_required
def mark_read(request, slug=None):
    notification_id = slug2id(slug)

    notification = get_object_or_404(
        Notification, recipient=request.user, id=notification_id)
    notification.mark_as_read()

    _next = request.GET.get('next')

    if _next:
        return redirect(_next)

    return JsonResponse('read', safe=False)


# {'friend_user__first_name': 'Dinesh',
#  'friend_user__last_name': 'Gawali',
#  'friend_user_id': 49}


@login_required(login_url="/")
def contacts(request):
    allusers = Friendship.objects.filter(current_user=request.user, is_active=True)

    alluser = list()
    for usr in allusers:
        usr_dict = dict()
        usr_dict["friend_user__first_name"] = usr.friend_user.first_name,
        usr_dict["friend_user__last_name"] = usr.friend_user.last_name,
        usr_dict["friend_user_id"] = usr.friend_user.id,
        usr_dict["online"] = 'true' if hasattr(usr.friend_user, 'logged_in_user') else 'false'

        alluser.append(usr_dict)
    return JsonResponse(list(alluser), safe=False)


@login_required(login_url="/")
def isonline(request):
    if request.method == "POST":
        request_dict = request.POST
        friend_usr = request_dict["friend_usr"]
        usr = User.objects.get(id=friend_usr)
        usr_dict = dict()
        usr_dict["online"] = 'true' if hasattr(usr, 'logged_in_user') else 'false'
    return JsonResponse(usr_dict, safe=False)
