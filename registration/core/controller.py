from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse

from registration.models import PendingFriendship, Friendship


def search_user_core(request, user_name):
    friends_data = list(User.objects.filter(Q(first_name__contains=user_name.lower()) |
                                            Q(last_name__contains=user_name.lower()) |
                                            Q(first_name__contains=user_name.title()) |
                                            Q(last_name__contains=user_name.title())).exclude(
        id=request.user.id).values())

    pending_request = PendingFriendship.objects.filter(from_user=request.user).values_list("to_user")
    pending_ids = [j[0] for j in pending_request]

    friends = Friendship.objects.filter(current_user=request.user).values_list("friend_user_id")
    friends_ids = [i[0] for i in friends]

    for fri_data in friends_data:
        if fri_data["id"] in friends_ids:
            fri_data["friend"] = True
            fri_data["pending"] = False

        elif fri_data["id"] in pending_ids:
            fri_data["friend"] = False
            fri_data["pending"] = True
        else:
            fri_data["friend"] = False
            fri_data["pending"] = False
    return JsonResponse(list(friends_data), safe=False)
