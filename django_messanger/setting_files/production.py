from .base import *
DEBUG = False
ALLOWED_HOSTS = ['*']

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DATABASE SETTINGS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django_messanger',
        'USER': 'root',
        'PASSWORD': 'password_password',
        'HOST': 'db',
        'PORT': '3306',
    }
}

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Django Channel Settings <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
ASGI_APPLICATION = "django_messanger.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('redis_server', 6379)],
        },
    },
}

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Django-Celery Settings <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis_server:6379',
    },
}

BROKER_URL = 'redis://redis_server:6379'
CELERY_RESULT_BACKEND = 'redis://redis_server:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
# CELERY_DEFAULT_QUEUE = "prod"
# CELERY_DEFAULT_ROUTING_KEY = "prod"
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Django_Notifications Settings <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


