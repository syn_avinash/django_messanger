import os
from corsheaders.defaults import default_headers

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SECRET_KEY = 'i1rlpc&gd=tt81jpc#&oul%zq1jcg7zks+^rmx6#zdluwuja&('

DEBUG = True

ALLOWED_HOSTS = ['*']
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'djcelery',
    'corsheaders',
    'registration',
    'chatapp',
    'channels',
    'rest_framework',
    'notifications',
    'celery'
]
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
]
ROOT_URLCONF = 'django_messanger.urls'

WSGI_APPLICATION = 'django_messanger.wsgi.application'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'), os.path.join(os.path.split(BASE_DIR)[0], 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> DATABASE SETTINGS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django_messanger',
        'USER': 'root',
        'PASSWORD': 'password',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': "django_messanger",
#         'USER': "root",
#         'PASSWORD': "password_password",
#         'HOST': "db",
#         'PORT': "3306"
#     }
# }
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Password validation<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> STATIC_URL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'

# STATIC_ROOT = os.path.join(BASE_DIR, "static")

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'), os.path.join(os.path.split(BASE_DIR)[0], 'static'))

MEDIA_ROOT = os.path.join(BASE_DIR, "media")

MEDIA_URL = "/media/"

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ]
}

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_HEADERS = ('x-requested-with', 'content-type', 'accept', 'origin', 'authorization', 'x-csrftoken')

# _______________________________________________________________________________________________________________________

ASGI_APPLICATION = "django_messanger.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [('127.0.0.1', 6379)],
        },
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'localhost:6379',
    },
}
BROKER_URL = 'redis://127.0.0.1:6379'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_DEFAULT_QUEUE = "local"
CELERY_DEFAULT_ROUTING_KEY = "local"

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Django_Notifications Settings <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
DJANGO_NOTIFICATIONS_CONFIG = {'SOFT_DELETE': True,'USE_JSONFIELD': True}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'formatters': {
        'verbose': {

            'format': '%(levelname)s | %(name)s | %(asctime)s | %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        # 'info_file': {
        #     'level': 'INFO',
        #     'class': 'logging.FileHandler',
        #     'filename': 'logs/info.log',
        #     'formatter': 'verbose'
        # },
        # 'debug_file': {
        #     'level': 'DEBUG',
        #     'class': 'logging.FileHandler',
        #     'filename': 'logs/debug.log',
        #     'formatter': 'verbose'
        # },
        'error_file': {

            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'logs/error.log',
            'formatter': 'verbose'
        },
        'request_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'logs/request.log',
            'formatter': 'verbose'
        },

        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            # 'filename': 'logs/console.log',
            'formatter': 'simple'
        },
        'celery': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'logs/celery.log',
            'formatter': 'simple',
            'maxBytes': 1024 * 1024 * 100,  # 100 mb
        },

    },
    'loggers': {
        # 'django': {
        #     'handlers': ['error_file', 'mail_admins'],
        #     'level': 'ERROR',
        #     'propagate': True,
        # },

        'django.request': {
            'handlers': ['error_file'],
            'level': 'ERROR',
            'propagate': True,
        },

        'chatapp': {
            'handlers': ['request_file', 'error_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'registration': {
            'handlers': ['request_file', 'error_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'celery': {
            'handlers': ['celery', 'console'],
            'level': 'DEBUG',
        },
    },
}

# #++++++++++++++++++++++++++++++++++ PRODUCTION SETTING+++++++++++++++++++++++++++++++++++++++
# ASGI_APPLICATION = "django_messanger.routing.application"
# CHANNEL_LAYERS = {
#     'default': {
#         'BACKEND': 'channels_redis.core.RedisChannelLayer',
#         'CONFIG': {
#             "hosts": [('redis_server', 6379)],
#         },
#     },
# }
#
# CACHES = {
#     'default': {
#         'BACKEND': 'django_redis.cache.RedisCache',
#         'LOCATION': 'redis_server:6379',
#     },
# }
# BROKER_URL = 'redis://redis_server:6379'
# CELERY_RESULT_BACKEND = 'redis://redis_server:6379'
# CELERY_ACCEPT_CONTENT = ['application/json']
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_RESULT_SERIALIZER = 'json'
# CELERY_DEFAULT_QUEUE = "local"
# CELERY_DEFAULT_ROUTING_KEY = "local"
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
