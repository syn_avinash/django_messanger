import notifications.urls
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'', include('registration.urls')),
    url(r'chat/', include('chatapp.urls')),
    url(r'^admin/', admin.site.urls),
    url('^inbox/notifications/', include(notifications.urls, namespace='notifications')),
]