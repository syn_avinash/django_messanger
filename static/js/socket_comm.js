<!--Script For Send and Recieve msg through Socket-->
var rec_user_id=$('input[name=requestuser]').val();
var rec_user_fname=$('input[name=requestfname]').val();
var rec_user_lname=$('input[name=requestlname]').val();
var roomName = "user_"+rec_user_id;

var chatSocket = new WebSocket('ws://'+ window.location.host +'/ws/chat/p/' + roomName + '/');
var online = new WebSocket('ws://'+ window.location.host +'/ws/chat/ol/online/' );
online.onmessage = function(e) {
    var on_data = JSON.parse(e.data);
    var on_usr=on_data['user']
    var on_status=on_data['is_logged_in']
    var on_fname=on_data['fname']
    var on_lname=on_data['lname']
    var on_msg_type=on_data['msg_type']
    var on_reciever=on_data['reciever']
    if($("#my_id div.chat_list[data-myval="+on_usr+"]").length >0){
        if(on_status=='true'){
            $("div.chat_list[data-myval="+on_usr+"] div div span").removeClass("offline")
//            $("#myModal div div.modal-header p").html('&nbsp;Online');
//            $("#myModal div div.modal-content p").html('&nbsp;'+on_fname+' '+on_lname);
//            $("#myModal").show();
//            $("#myModal").fadeOut(3000);
        }
        else{
            $("div.chat_list[data-myval="+on_usr+"] div div span").addClass("offline")

        }
    $("div.chat_list[data-myval="+on_usr+"]").attr('online',on_status);
    }
    if(on_reciever==rec_user_id && on_msg_type=='frrequest'){
       notice_html_data="<div class='alert alert-info' data-notid='none' data-usrreq="+on_usr+" data-verb='request'><strong class ='ufname'>"+on_fname+"</strong> <strong class ='ulname'>"+on_lname+"</strong><a href='#' class='btn btn-primary pull-right cnlreq' style='position: relative;bottom: 0px;margin-right: 6px;padding:0px 5px'>&#10006;</a><a href='#' class='btn btn-primary pull-right resreq' style='position: relative;bottom: 0px;margin-right: 6px;padding:0px 5px'>&#10003;</a></div>"
            $("#cl_noti_id").prepend(notice_html_data);
            var notif_div=$('#normal_count span.live_notify_badge');
            notif_div.html($("#cl_noti_id").children().length);
        }
    if(on_reciever==rec_user_id && on_msg_type=='req_accept'){
    var add_html="<div class='chat_list' data-myval="+on_usr+" online='true'><div class='chat_people'><div class='chat_img'><span class='online_icon'></span><img src='https://ptetutorials.com/images/user-profile.png' alt='AI'></div><div class='chat_ib'><b><h5 style='margin-top: 11px;'>"+on_fname+" "+on_lname+"<span class='chat_date'></span></h5></b></div></div></div>"
    $("#my_id").append(add_html);
    }
}

online.onopen = function(){

    online.send(JSON.stringify({'message': 'message','reciever':'all','msg_type':'online'}));

}
console.log('ws://'+ window.location.host +'/ws/chat/p/' + roomName + '/');

chatSocket.onmessage = function(e) {
    var data = JSON.parse(e.data);
    var message = data['message'];
    var reciever=data['reciever']
    var sender=String(data['sender'])
    var sender_name=String(data['sender_name'])
    var sender_lname=String(data['sender_lname'])

    if(($("button.tablinks[data-ref_id='chat_data_id']").hasClass('active') || $("#chat_data_id div.chat_list[data-myval="+sender+"]").hasClass('active_chat')) && $("#chat_data_id div.chat_list[data-myval="+sender+"]").length!=0){
        $("#chat_data_id div.chat_list[data-myval="+sender+"]").prependTo($("#chat_data_id"));
    }
    else{
        var contact_obj = $("div.chat_list[data-myval="+sender+"]").eq(0);
        $("#chat_data_id div.chat_list[data-myval="+sender+"]").detach();
        contact_obj.clone().removeClass("active_chat").prependTo($("#chat_data_id"));
    }
    var rec_usr1= String($('#my_id .chat_list.active_chat, #chat_data_id .chat_list.active_chat, #myTable tbody tr.chat_list.active_chat').data('myval'));
    if (rec_usr1==sender){
        var se_date=new Date().toLocaleString();
        nhtml="<div class='incoming_msg'><div class='incoming_msg_img'><img src='https://ptetutorials.com/images/user-profile.png'alt='image'></div><div class='received_msg'><div class='received_withd_msg'><p style='word-wrap: break-word;'>"+message+"</p><span class='time_date'>"+ se_date +"</span></div></div></div>"
        $("#demo1").append(nhtml)
        $("#demo1").scrollTop($("#demo1")[0].scrollHeight);
        }

    else{
        var usr = $("#chat_data_id .chat_list[data-myval="+sender+"]");
        $("#chat_data_id div.chat_list[data-myval="+sender+"]").css("background-color","yellow");
        $("#myModal div div.modal-header p").html('&nbsp;'+sender_name);
        $("#myModal div div.modal-content p").html('&nbsp;'+message);
        $("#myModal").show();
        $("#myModal").fadeOut(3000);

        if($("#cl_noti_id div[data-usrfrom="+sender+"]").length >0){
            var msgc = $("#cl_noti_id div[data-usrfrom="+sender+"] strong span").html();
            msgc++;
            $("#cl_noti_id div[data-usrfrom="+sender+"] strong span").html(msgc);
            $(usr).find('.chat_date').html("<h5><b>"+msgc+"</b></h5>");
        }
        else{
            var msgc=1;
            msg_noti_data="<div class='alert alert-info' data-notid='none' data-usrfrom="+sender+" data-verb='msg_noti'><strong><span>"+msgc+"</span> message from "+sender_name+" "+sender_lname+"</strong></div>"
            $("#cl_noti_id").prepend(msg_noti_data);
            var notif_div=$('#normal_count span.live_notify_badge');
            notif_div.html($("#cl_noti_id").children().length);
            $(usr).find('.chat_date').html("<h5><b>"+msgc+"</b></h5>");
        }
        var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
        $(document).ready(function () {
            $.ajax({url:'/chat/msg_noti/',
                    type:"POST",
                    data:{"sender":sender,"reciever":reciever,"csrfmiddlewaretoken":csrfmiddlewaretoken}
                    }).done(function(msg_data){
                });
            });
        }
};

document.querySelector('.write_msg').focus();
document.querySelector('.msg_send_btn').onclick = function(e) {
    var messageInputDom = document.querySelector('.write_msg');
    var message = messageInputDom.value;
    var display_message = messageInputDom.value.replace(/\n/g,'<br>');
    var rec_usr= String($('.chat_list.active_chat').data('myval'));
    var sen_date= new Date(Date.now()).toLocaleString();
    if(message.trim().length > 0){
            nhtml ="<div class='outgoing_msg'><div class='sent_msg'><p style='word-wrap: break-word;'>"+display_message+"</p><span class='time_date'>"+sen_date+"</span></div></div>"
            $("#demo1").append(nhtml)
            $("#demo1").scrollTop($("#demo1")[0].scrollHeight);

    if ($("#my_id div.chat_list[data-myval="+rec_usr+"]").attr('online')=='false'){
        var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
        $(document).ready(function () {
            $.ajax({url:'/chat/offline_msg/',
                type:"POST",
                data:{"sender":rec_user_id,"reciever":rec_usr,"message":message,
                "csrfmiddlewaretoken":csrfmiddlewaretoken}
                }).done(function(msg_data){});

        });
        }
    else{
        var recieverSocket= new WebSocket('ws://'+ window.location.host +'/ws/chat/p/user_'+ rec_usr + '/');
        recieverSocket.onopen = function() {
            recieverSocket.send(JSON.stringify({'message': message,'reciever':rec_usr,'sender':rec_user_id,'sender_name':rec_user_fname,'sender_lname':rec_user_lname}));
        };
    }

    var contact_obj = $("div.chat_list[data-myval="+rec_usr+"]").eq(0);
    var chat_obj = $("#chat_data_id div.chat_list[data-myval="+rec_usr+"]").detach();
    contact_obj.clone().prependTo($("#chat_data_id"));
    $("#chat_data_id div.chat_list[data-myval="+rec_usr+"]").addClass("active_chat");
    messageInputDom.value = '';
    }
}
