//Javascript function for get chat_id
function myFunction(a, b) {
	if (Number(a)<Number(b)){
    chat_url="user"+a+"_user"+b;
    }
    else{
    chat_url="user"+b+"_user"+a;
    }
    return chat_url;
}

// Jquery code for Shift Enter
function getCaret(el) {
  if (el.selectionStart) {
    return el.selectionStart;
  } else if (document.selection) {
    el.focus();

    var r = document.selection.createRange();
    if (r == null) {
      return 0;
    }

    var re = el.createTextRange(),
        rc = re.duplicate();
    re.moveToBookmark(r.getBookmark());
    rc.setEndPoint('EndToStart', re);

    return rc.text.length;
  }
  return 0;
}

