//Jquery Code For Show Search Result
$(document).ready(function () {
    $("#seInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $(".tablinks").removeClass("active");
        $("#clbtn").show();
        $(".type_msg").css("visibility", 'hidden');
        $("#demo1").html("<div style='margin: 30%;'><h1><center>Happy Searching</center></h1></div>");
        $.ajax({
            url:'/chat/search/'+value+'/',
            type:"GET",
            }).done(function(msg_data){
                var html="";
                var base_html="<h3>"+msg_data.length+" Records Found</h3><table class='table' id='myTable'><tbody>"
                $(msg_data).each(function(i,usr){
                if (usr.friend==true){
                    html+="<tr data-myval="+usr.id+"><td> <img src='https://ptetutorials.com/images/user-profile.png' alt='AI' class='chat_img'/>&nbsp;"+usr.first_name +" "+usr.last_name+"</td><td class='snd_msg'><button class='snd_msgb'>Send Message</button></td></tr>"
                }
                else if(usr.pending==true){
                    html+="<tr data-uid="+usr.id+"><td> <img src='https://ptetutorials.com/images/user-profile.png' alt='AI' class='chat_img'/>&nbsp;"+usr.first_name +" "+usr.last_name+"</td><td class='addfr'><button class='addfrb' disabled>Request Sent</button></td></tr>"
                }
                else{
                    html+="<tr data-uid="+usr.id+"><td> <img src='https://ptetutorials.com/images/user-profile.png' alt='AI' class='chat_img'/>&nbsp;"+usr.first_name +" "+usr.last_name+"</td><td class='addfr'><button class='addfrb'>Add Friend</button></td></tr>"
                }
                });
                newhtml=base_html+html+"</tbody></table>"
                $("#my_id, #chat_data_id, #cl_noti_id").hide();
                $("#search_friend_id").html(newhtml);
                $("#search_friend_id").show();
                });
    });
});


//Script for Send Friend Request-->
$(document).ready(function () {
    var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
    $("#search_friend_id").on("click", "td button.addfrb", function() {
    $(this).html("Request Sent");
    var friend_usr=$(this).closest("tr").data("uid");
    online.send(JSON.stringify({'message': 'message','reciever':friend_usr,'msg_type':'frrequest'}));
    $.ajax({url:'/chat/frreq/',
        type:"POST",
        data:{"request_to_user_id":friend_usr,"csrfmiddlewaretoken":csrfmiddlewaretoken}
        }).done(function(msg_data){
        });
   });
});


//jquery code or Get CHATDATA
$(document).ready(function() {
    $("#my_id, #chat_data_id").on("click", ".chat_list", function() {
        $(".chat_list").removeClass("active_chat");
        $(this).addClass("active_chat");
        var urec= $(this).data('myval');
        var user_id=$('input[name=requestuser]').val();
        var burl =myFunction(String(urec),user_id)

        // $('#cl_noti_id div[data-usrfrom='+urec+']').click();
        $(".type_msg").css("visibility", 'visible');
        $(this).css("background-color", "");
        $("#chat_data_id .chat_list[data-myval="+urec+"]").css("background-color", "");
        $("#chat_data_id .chat_list[data-myval="+urec+"]").find('.chat_date').html('');
        $.ajax({
	        url:'/chat/his/'+burl+'/',
            type:"GET",
        }).done(function(msg_data){
        $('#demo1').data(msg_data)
	   var html="";
	   $(msg_data).each(function(i,msg){
	       date_date= new Date(msg.create_date).toLocaleString();
	       if(Number(user_id)==msg.text_from_id){
	       html +="<div class='outgoing_msg'><div class='sent_msg'><p style='word-wrap: break-word;'>"+msg.text.replace(/\n/g,'<br>')+"</p><span class='time_date'>" +date_date+"</span></div></div>"
	       }
	       else
	       {
	       html+="<div class='incoming_msg'><div class='incoming_msg_img'><img src='https://ptetutorials.com/images/user-profile.png'alt='image'></div><div class='received_msg'><div class='received_withd_msg'><p style='word-wrap: break-word;'>"+msg.text.replace(/\n/g,'<br>')+"</p><span class='time_date'>" +date_date+"</span></div></div></div>"
	       }
	        });
        $("#demo1").html(html);
        $("#demo1").scrollTop($("#demo1")[0].scrollHeight);
    });

    if (($('#cl_noti_id div[data-usrfrom='+urec+']').length) >0){
        var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
        var noti_id=$('#cl_noti_id div[data-usrfrom='+urec+']').data("notid");
        var friend_usr=$('#cl_noti_id div[data-usrfrom='+urec+']').data("usrfrom");
        $.ajax({url:'/chat/delnotif/',
            type:"POST",
            data:{"friend_usr":friend_usr,"noti_id":noti_id,"csrfmiddlewaretoken":csrfmiddlewaretoken}
            }).done(function(msg_data){});
        $('#cl_noti_id div[data-usrfrom='+urec+']').remove();
        var notif_div=$('#normal_count span.live_notify_badge');
        notif_div.html($("#cl_noti_id").children().length);

    }

    });
});

// Jquery code for on tab click sub-div active and inactive hide and show
$(document).ready(function(){
  $('.tab').on('click', '.tablinks', function() {
     if($(this).hasClass('active')){
        return false
     }
     var ref_id=$(this).data("ref_id")
     $(".chat_list").removeClass(" active_chat")
     $(".tabcontent").css("display", "none");
     $("#"+ref_id).css("display", "block");
     $(".tablinks").removeClass(" active")
     $(this).addClass(" active");
     $("#demo1").html("");
     $(".type_msg").css("visibility", 'hidden');
     $("#search_friend_id").hide();
     $("#seInput").val('');
     $("#seInput").blur();
//     $("#seInput").attr("placeholder", "Type your answer here")
     $("#clbtn").hide();
    });
    });

//Script for search user if friend send msg-->
$(document).ready(function () {
    var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
    $("#search_friend_id").on("click", "td button.snd_msgb", function() {
    $(".chat_list").removeClass("active_chat");
    $(this).closest("tr").addClass("chat_list active_chat");
    var urec=$(this).closest("tr").data("myval");
    var user_id=$('input[name=requestuser]').val();
    var burl =myFunction(String(urec),user_id)
    $(".type_msg").css("visibility", 'visible');
    $.ajax({
	        url:'/chat/his/'+burl+'/',
            type:"GET",
        }).done(function(msg_data){
//        $('#demo1').data(msg_data)
	   var html="";
	   $(msg_data).each(function(i,msg){
	       date_date= new Date(msg.create_date).toLocaleString();
	       if(Number(user_id)==msg.text_from_id){
	       html +="<div class='outgoing_msg'><div class='sent_msg'><p style='word-wrap: break-word;'>"+msg.text.replace(/\n/g,'<br>')+"</p><span class='time_date'>" +date_date+"</span></div></div>"
	       }
	       else
	       {
	       html+="<div class='incoming_msg'><div class='incoming_msg_img'><img src='https://ptetutorials.com/images/user-profile.png'alt='image'></div><div class='received_msg'><div class='received_withd_msg'><p style='word-wrap: break-word;'>"+msg.text.replace(/\n/g,'<br>')+"</p><span class='time_date'>" +date_date+"</span></div></div></div>"
	       }
	        });
        $("#demo1").html(html);
        $("#demo1").scrollTop($("#demo1")[0].scrollHeight);
    });
    $("#chat_data_id .chat_list[data-myval="+urec+"]").css("background-color", "");
    $('.chat_date').html('');
    if (($('#cl_noti_id div[data-usrfrom='+urec+']').length) >0){
        var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
        var noti_id=$('#cl_noti_id div[data-usrfrom='+urec+']').data("notid");
        var friend_usr=$('#cl_noti_id div[data-usrfrom='+urec+']').data("usrfrom");
        $.ajax({url:'/chat/delnotif/',
            type:"POST",
            data:{"friend_usr":friend_usr,"noti_id":noti_id,"csrfmiddlewaretoken":csrfmiddlewaretoken}
            }).done(function(msg_data){});
        $('#cl_noti_id div[data-usrfrom='+urec+']').remove();
        var notif_div=$('#normal_count span.live_notify_badge');
        notif_div.html($("#cl_noti_id").children().length);
    }
   });
});


// Jquery code for see notifications
$(document).ready(function(){
  $("#id_notification1").click(function() {
     $.ajax({
        url:'/custom/notif/',
        type:"GET",
        }).done(function(noti_data){
        notice_html_data=""
        $(noti_data).each(function(i,notice){
        if(notice.verb=="request"){
        notice_html_data+="<div class='alert alert-info' data-notid="+notice.id+" data-usrreq="+notice.actor_object_id+" data-verb="+notice.verb+"><strong class ='ufname'>"+notice.first_name+"</strong> <strong class ='ulname'>"+notice.last_name+"</strong><a href='#' class='btn btn-primary pull-right cnlreq' style='position: relative;bottom: 0px;margin-right: 6px;padding:0px 5px'>&#10006;</a><a href='#' class='btn btn-primary pull-right resreq' style='position: relative;bottom: 0px;margin-right: 6px;padding:0px 5px'>&#10003;</a></div>"
        }
        else{
        notice_html_data+="<div class='alert alert-info' data-notid="+notice.id+" data-usrfrom="+notice.actor_object_id+" data-verb="+notice.verb+"><strong><span>"+notice.data+"</span> message from "+notice.first_name+" "+notice.last_name+"</strong></div>"
         }
        });
        $("#cl_noti_id").html(notice_html_data);
      });
    });
});

//JQUERY CODE FOR ADD FRIEND
$(document).ready(function () {
    $("#cl_noti_id").on("click", "div a.resreq", function() {
//    $(".resreq").click(function() {
    var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
    var noti_id=$(this).parent("div").data("notid");
    var friend_usr=$(this).parent("div").data("usrreq");
    var add_fname=$(this).siblings("strong.ufname").html();
    var add_lname=$(this).siblings("strong.ulname").html();
//    var add_html="<div class='chat_list' data-myval="+friend_usr+"><div class='chat_people'><div class='chat_img'><img src='https://ptetutorials.com/images/user-profile.png' alt='AI'></div><div class='chat_ib'><b><h5 style='margin-top: 11px;'>"+add_fname+" "+add_lname+"<span class='chat_date'></span></h5></b></div></div></div>"
    $.ajax({url:'/chat/addfr/',
            type:"POST",
            data:{"friend_usr":friend_usr,"noti_id":noti_id,"csrfmiddlewaretoken":csrfmiddlewaretoken}
            }).done(function(msg_data){});
    $.ajax({
        url:'/isonline/',
        data:{"friend_usr":friend_usr,"csrfmiddlewaretoken":csrfmiddlewaretoken},
        type:"POST",
        }).done(function(friend_data){
        add_html=""
        $(friend_data).each(function(i,friend){
        if (friend.online=='true'){
            add_html+="<div class='chat_list' data-myval="+friend_usr+" online="+friend.online+"><div class='chat_people'><div class='chat_img'><span class='online_icon'></span><img src='https://ptetutorials.com/images/user-profile.png' alt='AI'></div><div class='chat_ib'><b><h5 style='margin-top: 11px;'>"+add_fname+" "+add_lname+"<span class='chat_date'></span></h5></b></div></div></div>"
            }
        else{
            add_html+="<div class='chat_list' data-myval="+friend_usr+" online="+friend.online+"><div class='chat_people'><div class='chat_img'><span class='online_icon offline'></span><img src='https://ptetutorials.com/images/user-profile.png' alt='AI'></div><div class='chat_ib'><b><h5 style='margin-top: 11px;'>"+add_fname+" "+add_lname+"<span class='chat_date'></span></h5></b></div></div></div>"
            }
         $("#my_id").append(add_html);
            });
        });
    $(this).closest("div").remove();
    online.send(JSON.stringify({'message': 'message','reciever':friend_usr,'msg_type':'req_accept'}));
    var notif_div=$('#normal_count span.live_notify_badge');
    notif_div.html($("#cl_noti_id").children().length);
   });
});

//JQUERY CODE FOR CANCEL FRIEND REQUEST
$(document).ready(function () {
$("#cl_noti_id").on("click", "div a.cnlreq", function() {
//    $(".cnlreq").click(function() {
    var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
    var noti_id=$(this).parent("div").data("notid");
    var friend_usr=$(this).parent("div").data("usrreq");
    $.ajax({url:'/chat/cnlfr/',
            type:"POST",
            data:{"friend_usr":friend_usr,"noti_id":noti_id,"csrfmiddlewaretoken":csrfmiddlewaretoken}
            }).done(function(msg_data){
                });
    $(this).closest("div").remove();
    var notif_div=$('#normal_count span.live_notify_badge');
    notif_div.html($("#cl_noti_id").children().length);
   });
   });


// Jquery code for see notifications
$(document).ready(function(){
  $("button.tablinks[data-ref_id='my_id']").click(function() {
     $.ajax({
        url:'/friends/',
        type:"GET",
        }).done(function(friend_data){
        friend_html_data=""
        $(friend_data).each(function(i,friend){
        if (friend.online=='true'){
        friend_html_data+="<div class='chat_list' data-myval="+friend.friend_user_id+" online="+friend.online+"><div class='chat_people'><div class='chat_img'><span class='online_icon'></span><img src='https://ptetutorials.com/images/user-profile.png' alt='AI'></div><div class='chat_ib'><b><h5 style='margin-top: 11px;'>"+friend.friend_user__first_name+" "+friend.friend_user__last_name+"<span class='chat_date'></span></h5></b></div></div></div>"
        }
        else{
        friend_html_data+="<div class='chat_list' data-myval="+friend.friend_user_id+" online="+friend.online+"><div class='chat_people'><div class='chat_img'><span class='online_icon offline'></span><img src='https://ptetutorials.com/images/user-profile.png' alt='AI'></div><div class='chat_ib'><b><h5 style='margin-top: 11px;'>"+friend.friend_user__first_name+" "+friend.friend_user__last_name+"<span class='chat_date'></span></h5></b></div></div></div>"
        }
        });
        $("#my_id").html(friend_html_data);
      });
    });
});

// Jquery code for Shift Enter
$(document).ready(function(){
$('textarea').keyup(function (event) {
       if (event.keyCode == 13 && event.shiftKey) {
           var content = this.value;
           var caret = getCaret(this);
           this.value = content.substring(0,caret-1)+'\n'+content.substring(caret-1,content.length-1);
           event.stopPropagation();

      }else if(event.keyCode == 13)
      {
          document.querySelector('.msg_send_btn').click();
          this.value='';
      }
});
});

//JQUERY CODE FOR delete msg notification
$(document).ready(function () {
$("#cl_noti_id").on("click", "div[data-verb='msg_noti']", function() {
//    $(".cnlreq").click(function() {
    var csrfmiddlewaretoken = ($('input[name=csrfmiddlewaretoken]').val());
    var noti_id=$(this).data("notid");
    var friend_usr=$(this).data("usrfrom");
    $("button.tablinks[data-ref_id='chat_data_id']").click();
    $("#chat_data_id div[data-myval="+friend_usr+"]").click();
    $.ajax({url:'/chat/delnotif/',
            type:"POST",
            data:{"friend_usr":friend_usr,"noti_id":noti_id,"csrfmiddlewaretoken":csrfmiddlewaretoken}
            }).done(function(msg_data){
                });
    $(this).remove();
    var notif_div=$('#normal_count span.live_notify_badge');
    notif_div.html($("#cl_noti_id").children().length);
   });
});



// Jquery code for see notifications
$(document).ready(function(){
  $("#clbtn").click(function() {
     $("#search_friend_id").hide();
     $("#seInput").val('');
     $("#seInput").blur();
     $("button.tablinks[data-ref_id='chat_data_id']").click();
    });
});

$(window).on('load', function() {
 var da_ta=$("#cl_noti_id div[data-verb='msg_noti']");
 $(da_ta).each(function(i,dt){
    var msg_cnt= $(dt).find('strong span').html();
    var usr_id = $(dt).data('usrfrom');
    var usr = $("#chat_data_id .chat_list[data-myval="+usr_id+"]");
    $(usr).css("background-color", "yellow");
    $(usr).find('.chat_date').html("<h5><b>"+msg_cnt+"</b></h5>");
    });
});