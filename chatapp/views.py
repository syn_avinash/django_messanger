import json
import logging

from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.safestring import mark_safe

from chatapp.core.controller import msg_noti_core
from chatapp.models import *
from django.contrib.auth.models import User
from registration.core.controller import search_user_core
from registration.models import PendingFriendship, Friendship
from notifications.signals import notify
from notifications.models import Notification
from .tasks import store_data_task

logger = logging.getLogger(__name__)


# @login_required(login_url="/")
# def index(request):
#     return render(request, 'chatapp/index.html', {})


@login_required(login_url="/")
def room(request, room_name):
    return render(request, 'chatapp/room.html', {
        # 'room_name_json': mark_safe(json.dumps('groupchat'))
        'room_name_json': mark_safe(json.dumps(room_name))
    })


@login_required(login_url="/")
def personal_chat(request, room_name):
    return render(request, 'registration/home.html', {
        'room_name_json': mark_safe(json.dumps(room_name))
    })


@login_required(login_url="/")
def getdata(request, room_name):
    msg_data = ChattingData.objects.filter(chat_id=room_name).values()
    # data = model_to_dict(l)
    return JsonResponse(list(msg_data), safe=False)


@login_required(login_url="/")
def search_user(request, user_name):
    output = search_user_core(request, user_name)
    return output


@login_required(login_url="/")
def search(request):
    return render(request, 'chatapp/search.html', )


@login_required(login_url="/")
def friend_request(request):
    if request.method == "POST":
        request_to_user = request.POST["request_to_user_id"]
        to_user_id = User.objects.get(id=request_to_user)
        fr_obj, created = PendingFriendship.objects.get_or_create(from_user=request.user, to_user=to_user_id)
        if created:
            notify.send(request.user, recipient=to_user_id, verb="request", )
            return JsonResponse("request send", safe=False)
        if fr_obj and not created:
            return JsonResponse("request already send", safe=False)


@login_required(login_url="/")
def add_friend(request):
    if request.method == "POST":
        request_dict = request.POST
        friend_usr = request_dict["friend_usr"]
        noti_id = request_dict["noti_id"]
        Friendship.objects.get_or_create(current_user=request.user, friend_user_id=friend_usr)
        Friendship.objects.get_or_create(current_user_id=friend_usr, friend_user=request.user)
        pendingrequest = PendingFriendship.objects.get(to_user=request.user, from_user_id=friend_usr)
        pendingrequest.delete()
        if noti_id == 'none':
            notif = Notification.objects.get(recipient_id=request.user.id, actor_object_id=friend_usr,
                                             verb="request").delete()
        else:
            notif = Notification.objects.get(id=noti_id).delete()
            # notif = Notification.objects.get(id=noti_id)
            # notif.unread = False
            # notif.deleted = True
            # notif.save()
        return JsonResponse("response submitted", safe=False)


def store(request):
    if request.method == "POST":
        input_data = request.POST
        store_data_task.delay(input_data)
        # return True
        return JsonResponse("response submitted", safe=False)


@login_required(login_url="/")
def cnl_friend(request):
    if request.method == "POST":
        request_dict = request.POST
        friend_usr = request_dict["friend_usr"]
        noti_id = request_dict["noti_id"]
        pendingrequest = PendingFriendship.objects.get(to_user=request.user, from_user_id=friend_usr)
        pendingrequest.delete()
        if noti_id == 'none':
            notif = Notification.objects.get(recipient_id=request.user.id, actor_object_id=friend_usr,
                                             verb="request").delete()
        else:
            notif = Notification.objects.get(id=noti_id).delete()
            # notif = Notification.objects.get(id=noti_id)
            # notif.unread = False
            # notif.deleted = True
            # notif.save()
        return JsonResponse("response submitted", safe=False)


@login_required(login_url="/")
def msg_noti(request):
    if request.method == "POST":
        request_dict = request.POST
        sender = request_dict["sender"]
        reciever = request_dict["reciever"]
        output = msg_noti_core(sender, reciever)
        return output
    else:
        return JsonResponse("error", safe=False)


@login_required(login_url="/")
def del_notif_msg(request):
    if request.method == "POST":
        request_dict = request.POST
        friend_usr = request_dict["friend_usr"]
        noti_id = request_dict["noti_id"]
        if noti_id == 'none':
            notif = Notification.objects.get(recipient_id=request.user.id, actor_object_id=friend_usr,
                                             verb="msg_noti").delete()
        else:
            notif = Notification.objects.get(id=noti_id).delete()
        return JsonResponse("response submitted", safe=False)


@login_required(login_url="/")
def offline_msg(request):
    if request.method == "POST":
        request_dict = request.POST
        sender = request_dict["sender"]
        reciever = request_dict["reciever"]
        message = request_dict["message"]
        text_data_json = {'message': message, 'sender': sender, 'reciever': reciever}
        store_data_task.delay(text_data_json)
        msg_noti_core(sender, reciever)
        return JsonResponse("response submitted", safe=False)
    else:
        return JsonResponse("response submitted", safe=False)
