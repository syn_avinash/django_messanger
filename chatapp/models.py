from django.db import models
from django.contrib.auth.models import User
from registration.models import TrackDateModel


class ChattingData(TrackDateModel):
    chat_id = models.CharField(max_length=120, db_index=True)
    text = models.CharField(max_length=255)
    text_from = models.ForeignKey(User, related_name="text_from")
    text_to = models.ForeignKey(User, related_name="text_to")
    block = models.NullBooleanField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.chat_id
