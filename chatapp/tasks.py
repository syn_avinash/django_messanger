from celery import shared_task
from celery.utils.log import get_task_logger

from chatapp.core.controller import store_data, login_status, logout_status

logger = get_task_logger(__name__)


@shared_task(name='store_data_task')
def store_data_task(event):
    print("In ------>>>>  STORE DATA")
    return store_data(event)


@shared_task(name='login_status_task')
def login_status_task(event):
    print("In ------>>>>  user_logged_in")
    return login_status(event)


@shared_task(name='logout_status_task')
def logout_status_task(event):
    print("In ------>>>>  logout or seesion out")
    return logout_status(event)
