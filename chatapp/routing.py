# chatapp/routing.py
from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/chat/ol/online/$', consumers.online_user),
    url(r'^ws/chat/p/(?P<room_name>[^/]+)/$', consumers.PrivateChatConsumer),

]
