from django.test import TestCase, RequestFactory
from chatapp.models import ChattingData
from django.contrib.auth.models import User

from chatapp.views import getdata
from django.test import Client


class ChattingDataTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        User.objects.create(username="avinash", first_name="Avinash", last_name="Thorat")
        User.objects.create(username="dinesh", first_name="Dinesh", last_name="Gawali")
        self.usr1 = User.objects.get(username="avinash")
        usr2 = User.objects.get(username="dinesh")
        ChattingData.objects.create(chat_id="user1_user2", text="roar", text_from=self.usr1, text_to=usr2)

    def test_chatting_data(self):
        ch_data = ChattingData.objects.get(chat_id="user1_user2")
        self.assertTrue(isinstance(ch_data, ChattingData))
        self.assertEqual(ch_data.__str__(), ch_data.chat_id)

    def test_chatting_status_code(self):
        request = self.factory.get('/chat/his/user1_user2')
        request.user = self.usr1
        response = getdata(request, 'user1_user2')
        self.assertEqual(response.status_code, 200)

    # def test_store_chat_data(self):
    #     # request = self.factory.post('/chat/store/')
    #     # request.user = self.usr1
    #     # request.POST = {"message": "AAAAAAAAAAAAAAAAAAAAAAAAAA", "reciever": 8, "sender": 7}
    #     # response = store(request)
    #     # print(response)
    #     # -----------------------------------------------------------------------------------
    #     client=Client()
    #     response = client.post('/chat/store/', {"message": "bbbbbbbbbbbbbbbbbbbbbbbbbb", "reciever": 8, "sender": 7})
    #     self.assertEqual(response.status_code, 200)
