# chatapp/urls.py
from django.conf.urls import url
from . import views

urlpatterns = [
    # url(r'^$', views.index, name='index'),
    # url(r'^groupchat/$', views.room, name='room'),
    url(r'^gr/(?P<room_name>[^/]+)/$', views.room, name='room'),
    url(r'^p/(?P<room_name>[^/]+)/$', views.personal_chat),
    # url(r'^user7_user8/$', views.getdata, name=''),
    url(r'^his/(?P<room_name>[^/]+)/$', views.getdata),
    url(r'^search/(?P<user_name>[^/]+)/$', views.search_user),
    url(r'^search/$', views.search),
    url(r'^frreq/$', views.friend_request),
    url(r'^addfr/$', views.add_friend),
    url(r'^store/$', views.store),
    url(r'^cnlfr/$', views.cnl_friend),
    url(r'^msg_noti/$', views.msg_noti),
    url(r'^delnotif/$', views.del_notif_msg),
    url(r'^offline_msg/$', views.offline_msg),
]
