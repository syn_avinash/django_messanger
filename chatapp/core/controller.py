from django.db.models import F
from notifications.models import Notification

from chatapp.models import ChattingData
from registration.models import LoggedInUser
from django.http import JsonResponse


def create_chat_id(a, b):
    if int(a) < int(b):
        chatid = "user{}_user{}".format(a, b)
    else:
        chatid = "user{}_user{}".format(b, a)
    return chatid


def store_data(event):
    message = event["message"]
    reciever = event["reciever"]
    sender = event["sender"]
    user_chat_id = create_chat_id(sender, reciever)
    ChattingData.objects.create(chat_id=user_chat_id, text=message, text_from_id=sender, text_to_id=reciever)
    return True


def login_status(event):
    LoggedInUser.objects.get_or_create(user_id=event)
    return True


def logout_status(event):
    LoggedInUser.objects.filter(user_id=event).delete()
    return True


def msg_noti_core(sender, reciever):
    get, created = Notification.objects.get_or_create(actor_object_id=int(sender), recipient_id=reciever,
                                                      verb="msg_noti", actor_content_type_id=2, unread=True)
    if not created:
        get.data = F('data') + 1
        get.save()
    else:
        get.data = 1
        get.save()

    return JsonResponse("response submitted", safe=False)
