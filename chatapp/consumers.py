import json

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from chatapp.tasks import store_data_task, login_status_task, logout_status_task


class online_user(WebsocketConsumer):
    def connect(self):
        # self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'online_users'
        self.user_id = self.scope['user'].id
        self.fname = self.scope['user'].first_name
        self.lname = self.scope['user'].last_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(self.room_group_name, self.channel_name)
        self.accept()
        login_status_task.delay(self.user_id)

    def disconnect(self, close_code):
        logout_status_task.delay(self.user_id)
        async_to_sync(self.channel_layer.group_send)(self.room_group_name, {'type': 'chat_message',
                                                                            'user': self.user_id.__str__(),
                                                                            'fname': self.fname,
                                                                            'lname': self.lname,
                                                                            'is_logged_in': 'false',
                                                                            'reciever': 'all',
                                                                            'msg_type': 'online'})

        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(self.room_group_name, self.channel_name)

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        msg_type = text_data_json['msg_type']
        reciever = text_data_json['reciever']
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(self.room_group_name, {'type': 'chat_message',
                                                                            'user': self.user_id.__str__(),
                                                                            'fname': self.fname,
                                                                            'lname': self.lname,
                                                                            'is_logged_in': 'true',
                                                                            'reciever': reciever,
                                                                            'msg_type': msg_type})

    # Receive message from room group
    def chat_message(self, event):
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))


class PrivateChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name, self.channel_name)

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        reciever = text_data_json['reciever']
        sender = text_data_json['sender']
        sender_name = text_data_json['sender_name']
        sender_lname = text_data_json['sender_lname']
        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(self.room_group_name, {'type': 'chat_message', 'message': message,
                                                                            'sender': sender, 'reciever': reciever,
                                                                            'sender_name': sender_name,
                                                                            'sender_lname': sender_lname, })
        store_data_task.delay(text_data_json)

    # Receive message from room group
    def chat_message(self, event):
        message = event['message']
        # Send message to WebSocket
        self.send(text_data=json.dumps(event))
